# My switch SD setup

Every time I need to update the homebrew on my switch or install something new
I've forgotten how my SD card is set up. I decided to turn the setup into a
bash script so I won't forget.

## Usage

	./switch-sd-setup.sh <path to mounted SD card>

## todo

* hekate
* atmosphere
* 90DNS tester
* daybreak
* moonlight
* retroarch
