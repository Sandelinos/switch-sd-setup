#!/usr/bin/env bash
sysclk_version="2.0.0-rc"
ovlmenu_version="v1.2.3"
ovlloader_version="v1.0.7"

BYELLOW='\033[33;1m'
YELLOW='\033[33m'
RED='\033[31m'
NC='\033[0m'

[ -d "$1" ] || {
	echo "Usage: $0 <path to mounted SD card>"
	exit 1
}

[ -n "$XDG_CACHE_HOME" ] \
	&& cachedir="$XDG_CACHE_HOME/switch-sd/" \
	|| cachedir="$HOME/.cache/switch-sd/"
mkdir -p "$cachedir"

dl_and_extract() {
	local software="$1"
	local version="$2"
	local url="$3"
	if [ -f "$cachedir/$software-$version.zip" ]; then
		printf '%b%s %s already downloaded.%b\n' "$YELLOW" "$software" "$version" "$NC"
	else
		printf '%bDownloading %s %s...%b\n' "$YELLOW" "$software" "$version" "$NC"
		curl -L "$url" -o "$cachedir/$software-$version.zip" || {
			printf '%bCouldn'"'"'t download %s!%b\n' "$RED" "$software" "$NC"
			return 1
		}
	fi

	if [ -d "$cachedir/$software-$version/" ]; then
		printf '%b%s %s already extracted.%b\n' "$YELLOW" "$software" "$version" "$NC"
	else
		printf '%bExtracting %s %s...%b\n' "$YELLOW" "$software" "$version" "$NC"
		unzip "$cachedir/$software-$version.zip" \
			-d "$cachedir/$software-$version/" || {
				printf '%bCouldn'"'"'t unzip %s!%b\n' "$RED" "$software" "$NC"
				return 1
		}
	fi
}

# nx-ovlloader
dl_and_extract ovlloader "$ovlloader_version" "https://github.com/WerWolv/nx-ovlloader/releases/download/$ovlloader_version/nx-ovlloader.zip" && {
	printf '%bInstalling ovlloader...%b\n' "$BYELLOW" "$NC"
	rsync -cr "$cachedir/ovlloader-$ovlloader_version/atmosphere/" "$1/atmosphere/"
}

# Tesla-Menu
dl_and_extract ovlmenu "$ovlmenu_version" "https://github.com/WerWolv/Tesla-Menu/releases/download/$ovlmenu_version/ovlmenu.zip" && {
	printf '%bInstalling ovlmenu...%b\n' "$BYELLOW" "$NC"
	rsync -cr "$cachedir/ovlmenu-$ovlmenu_version/switch/" "$1/switch/"
}

# sys-clk
dl_and_extract sys-clk "$sysclk_version" "https://github.com/retronx-team/sys-clk/releases/download/$sysclk_version/sys-clk-${sysclk_version}_4.zip" && {
	printf '%bInstalling sys-clk...%b\n' "$BYELLOW" "$NC"
	rsync -cr "$cachedir/sys-clk-$sysclk_version/atmosphere/" "$1/atmosphere/"
	rsync -cr "$cachedir/sys-clk-$sysclk_version/switch/"     "$1/switch/"
}
